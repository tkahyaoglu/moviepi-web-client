package listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import firebase.FirebaseUtils;

public class WatchContextListener implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent arg0) {
        System.out.println("ServletContextListener destroyed");
    }

    // Run this before web application is started
    public void contextInitialized(ServletContextEvent sce){
        
        FirebaseUtils.obtainToken();
        System.out.println("ServletContextListener started");
        
    }
}