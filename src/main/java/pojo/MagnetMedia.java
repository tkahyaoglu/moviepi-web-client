package pojo;

public class MagnetMedia extends Media {
    public MagnetMedia() {
        super();
        this.setMediaType("magnet");
    }
    public MagnetMedia(String url) {
        super(url);
        this.setMediaType("magnet");
    }
}
