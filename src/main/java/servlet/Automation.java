package servlet;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import pojo.MagnetMedia;
import firebase.FirebaseUtils;

/**
 * Servlet implementation class for Servlet: Automation Servlet
 * 
 */
public class Automation extends javax.servlet.http.HttpServlet implements
        javax.servlet.Servlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Automation() {
        super();
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        System.out.println("Token: " + FirebaseUtils.getToken());

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);

        // response.getWriter().write("Hello World");

    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // VelocityEngine ve = new VelocityEngine();
        // Properties props = new Properties();

        String name = request.getParameter("name");

        name = StringEscapeUtils.escapeHtml4(name);

        Document doc = Jsoup.connect(
                "http://startorrents.com/search/all/" + name
                        + "/verified/sortby:seeders:D/").get();
        // Elements magnetLinkElements = doc.select(".icons a[href]");
        // Elements titleElements = doc.select(".item a[href]");
        String magnetUri = "";// magnetLinkElements.first().attr("href");
        String title = "";// titleElements.eq(1).attr("title");
        Elements linkElements = doc.select(".table-tbody tr");
        int size = 5;
        if (linkElements.size() < size) {
            size = linkElements.size();
        }
        for (int i = 0; i < size; i++) {
            Element e = linkElements.get(i);
            title = e.select(".item a[href]").eq(1).attr("title");
            magnetUri = e.select(".icons a[href]").first().attr("href");
            // e.select(".peer").first().text();
        }

        String token = FirebaseUtils.getToken();
        // System.out.println(token);

        System.out.println(FirebaseUtils.getQueue(token));

        MagnetMedia media = new MagnetMedia(magnetUri);
        media.setCommand("download");
        media.setName(title);
        media.setTitle(title);

        System.out.println(media.toJsonString());
        System.out
                .println(StringEscapeUtils.unescapeJava(media.toJsonString()));

        // VelocityContext context = new VelocityContext();
        // context.put("media", media);

        // Template t = ve.getTemplate("WEB-INF/templates/index.vm");
        // StringWriter writer = new StringWriter();
        // t.merge(context, writer);
        // response.getWriter().write(writer.toString());

        FirebaseUtils.addToQueue(media, token);

        request.setAttribute("media", media);

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
        // response.getWriter().write("title " + title + " magnet:" +
        // magnetUri);
    }

    private void startScheduler() {
        SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
        Scheduler sched;
        try {
            sched = schedFact.getScheduler();

            sched.start();
            // define the job and tie it to our HelloJob class
            JobDetail job = JobBuilder.newJob().withIdentity("myJob", "group1")
                    .build();
            // Trigger the job to run now, and then every 40 seconds
            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("myTrigger", "group1")
                    .startNow()
                    .withSchedule(
                            SimpleScheduleBuilder.simpleSchedule()
                                    .withIntervalInSeconds(40).repeatForever())
                    .build();
            // Tell quartz to schedule the job using our trigger
            sched.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}