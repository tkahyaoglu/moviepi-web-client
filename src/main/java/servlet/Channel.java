package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import firebase.FirebaseUtils;

/**
 * Servlet implementation class for Servlet: Channel Servlet
 * 
 */
public class Channel extends javax.servlet.http.HttpServlet
        implements javax.servlet.Servlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Channel() {
        super();
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        String token = FirebaseUtils.getToken();
        System.out.println("Token: " + token);

        request.setAttribute("connected",
                FirebaseUtils.isConnected(token).toString());
        request.setAttribute("deviceSerial", FirebaseUtils.getDEVICE_SERIAL());
        request.setAttribute("deviceId", FirebaseUtils.getDEVICE_ID());

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/default.jsp");
        dispatcher.forward(request, response);

        // response.getWriter().write("Hello World");
    }
}