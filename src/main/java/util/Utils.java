package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import pojo.MagnetMedia;
import pojo.Media;

public class Utils {
    public static String DEFAULT_ICON_URL = "http://t0.gstatic.com/images?q=tbn:ANd9GcQq3pIz-aKgkmYX1dJ-EL-AlHSPcOO7wdqRIJ5gJy9qNinXpmle";
    public static String DEFAULT_CONTAINER_URL = "http://kudu-test.s3-website-eu-west-1.amazonaws.com";

    private Utils() {

    }

    // HTTP GET request
    public static JsonObject sendGet(URI uri) throws Exception {

        HttpClient client = HttpClientBuilder.create().build();

        HttpGet request = new HttpGet(uri);

        request.setURI(uri);

        // add request header
        request.addHeader("User-Agent", "Mozilla/5.0");
        HttpResponse response = client.execute(request);

        System.out.println(
                "\nSending 'GET' request to URL : " + uri.toURL().toString());
        System.out.println(
                "Response Code : " + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        JsonObject j = new JsonObject();

        if (!result.toString().equals("null")) {
            try {
                j = new JsonParser().parse(result.toString()).getAsJsonObject();
            } catch (IllegalStateException ex) {
                j.addProperty("value", result.toString());
            }
        }

        System.out.println(result.toString());

        return j;
    }

    public static JsonObject sendPut(URI uri, Media media) throws Exception {
        HttpClient httpClient = HttpClientBuilder.create().build();

        System.out.println(
                "\nSending 'PUT' request to URL : " + uri.toURL().toString());

        HttpPut putRequest = new HttpPut(uri);
        putRequest.addHeader("Content-Type", "application/json");

        StringEntity input = null;
        try {
            String unescapedJson = StringEscapeUtils
                    .unescapeJava(media.toJsonString());
            input = new StringEntity(unescapedJson);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        putRequest.setEntity(input);

        HttpResponse response = httpClient.execute(putRequest);

        System.out.println(
                "Response code:" + response.getStatusLine().getStatusCode());
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        JsonObject j = new JsonParser().parse(result.toString())
                .getAsJsonObject();

        System.out.println("Result is " + result.toString());
        return j;
    }

    public static String createId(String url) {

        StringBuffer idString = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] mediaId = md.digest(url.getBytes("UTF-8"));
            for (byte b : mediaId) {
                idString.append(String.format("%02x", b & 0xff));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return idString.toString();
    }

    public static Media getMagnetMediaFromName(String name) throws IOException {
        Document doc = Jsoup.connect("http://startorrents.com/search/all/"
                + name + "/verified/sortby:seeders:D/").get();
        // Elements magnetLinkElements = doc.select(".icons a[href]");
        // Elements titleElements = doc.select(".item a[href]");

        String magnetUri = "";// magnetLinkElements.first().attr("href");
        String title = "";// titleElements.eq(1).attr("title");
        Elements linkElements = doc.select(".table-tbody tr");
        int size = 5;
        if (linkElements.size() < size) {
            size = linkElements.size();
        }
        for (int i = 0; i < size; i++) {
            Element e = linkElements.get(i);
            title = e.select(".item a[href]").eq(1).attr("title");
            magnetUri = e.select(".icons a[href]").first().attr("href");
        }

        MagnetMedia media = new MagnetMedia(magnetUri);
        media.setCommand("download");
        media.setName(title);
        media.setTitle(title);

        return media;
    }

}
