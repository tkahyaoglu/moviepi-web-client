package pojo;

import util.Utils;

import com.google.gson.Gson;

public class Media {
    private String command;
    private String containerUrl;
    private String downloaded;
    private String files;
    private String history;
    private String icon;
    private String id;
    private String mediaType;
    private String name;
    private String path;
    private String speed;
    private String title;
    private String tmp;
    private String total;
    private String url;
    private String subtitleUrl;

    public Media() {
        this.setContainerUrl(Utils.DEFAULT_CONTAINER_URL);
        this.setIcon(Utils.DEFAULT_ICON_URL);
    }

    public Media(String url) {
        this.setContainerUrl(Utils.DEFAULT_CONTAINER_URL);
        this.setIcon(Utils.DEFAULT_ICON_URL);
        this.setUrl(url);
        this.setPath("/home/pi/kududownload/media/" + id);
        this.setTmp("/home/pi/kududownload/tmp/" + id);
        this.setHistory("false");
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getContainerUrl() {
        return containerUrl;
    }

    public void setContainerUrl(String containerUrl) {
        this.containerUrl = containerUrl;
    }

    public String getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(String downloaded) {
        this.downloaded = downloaded;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id != null) {
            this.id = id;
        }
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTmp() {
        return tmp;
    }

    public void setTmp(String tmp) {
        this.tmp = tmp;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        String id = Utils.createId(url);
        this.id = id;
    }

    public String getSubtitleUrl() {
        return subtitleUrl;
    }

    public void setSubtitleUrl(String subtitleUrl) {
        this.subtitleUrl = subtitleUrl;
    }

    public String toJsonString() {
        Gson gson = new Gson();
        String json = gson.toJson(this);

        return json;
    }
}
