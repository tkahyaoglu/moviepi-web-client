package rest;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;

import pojo.MagnetMedia;
import pojo.Media;
import pojo.VideoMedia;
import util.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import firebase.FirebaseUtils;

@Path("/")
public class Kudufy {
    @Path("/magnet/download")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response downloadMagnet(String data) throws JSONException {

        Gson gson = new Gson();
        System.out.println(data);
        JsonObject obj = new JsonParser().parse(data).getAsJsonObject();
        MagnetMedia m = gson.fromJson(data, MagnetMedia.class);
        m.setUrl(obj.get("url").getAsString());
        System.out.println(m.toJsonString());

        FirebaseUtils.addToQueue(m, FirebaseUtils.getToken());

        String result = "@Produces(\"application/json\") Output: \n\nOutput: \n\n"
                + m.toJsonString();
        return Response.status(200).entity(result).build();
    }

    @Path("/magnet/watch")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response watchMagnet(String data) throws JSONException {

        Gson gson = new Gson();
        System.out.println(data);
        JsonObject obj = new JsonParser().parse(data).getAsJsonObject();
        MagnetMedia m = gson.fromJson(data, MagnetMedia.class);
        m.setUrl(obj.get("url").getAsString());
        System.out.println(m.toJsonString());

        FirebaseUtils.playMedia(m, FirebaseUtils.getToken());

        String result = "Watch @Produces(\"application/json\") Output: \n\nOutput: \n\n"
                + m.toJsonString();
        return Response.status(200).entity(result).build();
    }

    @Path("/video/watch")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response watchVideo(String data) throws JSONException {

        Gson gson = new Gson();
        System.out.println(data);
        JsonObject obj = new JsonParser().parse(data).getAsJsonObject();
        VideoMedia m = gson.fromJson(data, VideoMedia.class);
        m.setUrl(obj.get("url").getAsString());
        System.out.println(m.toJsonString());

        FirebaseUtils.playMedia(m, FirebaseUtils.getToken());

        String result = "Watch @Produces(\"application/json\") Output: \n\nOutput: \n\n"
                + m.toJsonString();
        return Response.status(200).entity(result).build();
    }

    @Path("/video/findanddownlad")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAndDownload(String data) throws JSONException,
            IOException {

        JsonObject obj = new JsonParser().parse(data).getAsJsonObject();

        String name = obj.get("name").getAsString();

        name = StringEscapeUtils.escapeHtml4(name);

        Media media = Utils.getMagnetMediaFromName(name);

        String token = FirebaseUtils.getToken();
        System.out.println(FirebaseUtils.getQueue(token));

        System.out
                .println(StringEscapeUtils.unescapeJava(media.toJsonString()));

        FirebaseUtils.addToQueue(media, token);

        String result = "Watch @Produces(\"application/json\") Output: \n\nOutput: \n\n"
                + media.toJsonString();
        return Response.status(200).entity(result).build();
    }
}