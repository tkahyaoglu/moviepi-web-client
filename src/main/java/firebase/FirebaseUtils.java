package firebase;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.NoSuchAlgorithmException;

import org.apache.http.client.utils.URIBuilder;

import pojo.Media;
import util.Utils;

import com.google.gson.JsonObject;

public class FirebaseUtils {
    static private String DEVICE_ID = "TVMJYPZ";
    static private String DEVICE_SERIAL = "25b605b0-0995-4b2d-b097-0f4e027178c9";
    static private String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhZG1pbiI6ZmFsc2UsImV4cCI6MTQ4MDg1MzYwOCwidiI6MCwiZCI6eyJ1aWQiOiJUVk1KWVBaIiwiZGV2aWNlSWQiOiJUVk1KWVBaIiwiZGV2aWNlU2VyaWFsIjoiMjViNjA1YjAtMDk5NS00YjJkLWIwOTctMGY0ZTAyNzE3OGM5In0sImlhdCI6MTQ0OTIzMTIwOH0.7eEPOvgBRT1JJ1nZxoOax8fsgaRO1EeNFXSstNvOoGM";
    static private String FIREBASE_URL = "https://moviepi.firebaseio.com";

    public static String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public static void setDEVICE_ID(String dEVICE_ID) {
        DEVICE_ID = dEVICE_ID;
    }

    public static String getDEVICE_SERIAL() {
        return DEVICE_SERIAL;
    }

    public static void setDEVICE_SERIAL(String dEVICE_SERIAL) {
        DEVICE_SERIAL = dEVICE_SERIAL;
    }

    public static String getToken() {
        return token;
    }

    public static void obtainToken() {
        try {
            if (token == null || token.equals("")) {
                JsonObject j = Utils.sendGet(
                        new URIBuilder("http://boxe.azurewebsites.net/token")
                                .addParameter("deviceId", DEVICE_ID)
                                .addParameter("deviceSerial", DEVICE_SERIAL)
                                .build());
                token = j.get("token").getAsString();
            } else {
                System.out.println("Token already exist");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String getQueue(String token) {
        JsonObject j = null;
        try {

            j = Utils.sendGet(new URIBuilder(
                    FIREBASE_URL + "/download/" + DEVICE_ID + "/completed.json")
                            .addParameter("auth", token).build());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return j.toString();
    }

    public static void addToQueue(Media media, String token) {
        try {
            URI uri = new URIBuilder(FIREBASE_URL + "/download/" + DEVICE_ID
                    + "/queue/" + media.getId() + ".json")
                            .addParameter("auth", token).build();

            Utils.sendPut(uri, media);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean isConnected(String token) {
        JsonObject j = null;
        try {
            URI uri = new URIBuilder(
                    FIREBASE_URL + "/online/" + DEVICE_ID + ".json")
                            .addParameter("auth", token).build();

            j = Utils.sendGet(uri);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Boolean isConnected = false;
        if (j != null) {
            isConnected = j.get("value").getAsBoolean();
        }

        return isConnected;
    }

    public static void playMedia(Media media, String token) {
        try {
            URI uri = new URIBuilder(
                    FIREBASE_URL + "/tvCommand/" + DEVICE_ID + ".json")
                            .addParameter("auth", token).build();

            Utils.sendPut(uri, media);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
