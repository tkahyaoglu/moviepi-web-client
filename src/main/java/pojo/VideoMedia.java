package pojo;

public class VideoMedia extends Media {
    public VideoMedia() {
        super();
        this.setMediaType("youtube");
    }
    public VideoMedia(String url) {
        super(url);
        this.setMediaType("youtube");
    }
}
