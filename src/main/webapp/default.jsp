<html>
<head>

<title>Kudu Tansu Channels</title>

<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ=="
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"
	integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"
	integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ=="
	crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#channel">Channel</a></li>
		<li><a data-toggle="tab" href="#settings">Settings</a></li>
	</ul>

	<div class="tab-content">
		<div id="channel" class="tab-pane fade in active">
			<jsp:include page="channel.jsp">
				<jsp:param value="${connected}" name="isConnected" />
			</jsp:include>
		</div>
		<div id="settings" class="tab-pane fade">
			<jsp:include page="settings.jsp">
				<jsp:param value="${connected}" name="isConnected" />
				<jsp:param value="${deviceId}" name="deviceId" />
				<jsp:param value="${deviceSerial}" name="deviceSerial" />
			</jsp:include>
		</div>
	</div>

</body>
</html>