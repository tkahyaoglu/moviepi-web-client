
<%
    String isConnected = (String) request.getParameter("isConnected");
%>
<%
    String deviceId = (String) request.getParameter("deviceId");
%>
<%
    String deviceSerial = (String) request.getParameter("deviceSerial");
%>
<div class="page-header">
	<h1>Settings</h1>
</div>
<div>
	<h5>
		Kudu Status
		<%
	    if (isConnected.equals("true")) {
	%>
		<span class="label label-success">Connected</span>
		<%
		    } else {
		%>
		<span class="label label-danger">Disconnected</span>
		<%
		    }
		%>
	</h5>
	<form name="settings">
		<div>
			Device ID: <input type="text" class="form-control" name="deviceId"
				id="deviceId" <%if (deviceId != null) {%> value=<%=deviceId%> <%}%> />
			<br>
		</div>
		<div>
			Device Serial: <input type="text" class="form-control"
				name="deviceSerial" id="deviceSerial"
				<%if (deviceSerial != null) {%> value=<%=deviceSerial%> <%}%> /> <br>
		</div>
	</form>
	<input id="saveSettings" class="btn btn-default" type="button"
		name="Save" value="Save" />
</div>
