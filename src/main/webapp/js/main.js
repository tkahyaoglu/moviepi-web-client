$(function() {

			$(".openpage").click(function() {
				moviepi.send({
					command : 'openPage',
					url : $(this).attr('data-url'),
					title : $(this).text()
				});
			});

			$('#watchMagnet').click(function() {
				var strng = {
					"url" : $("#mgn").val(),
					"title" : getName(),
					"name" : getName(),
					"command" : "play",
					"history" : true,
					"subtitleUrl" : jQuery('#srt').val()
				};
				playMagnet(strng);
			});
			$('#downloadMagnet').click(function() {
				var strng = {
					"url" : $("#mgn").val(),
					"title" : getName(),
					"name" : getName(),
					"command" : "download"
				};
				downloadMagnet(strng);
			});
			$('#playVideo').click(function() {
				var strng = {
					"url" : $("#videoLink").val(),
					"title" : "Youtube Link",
					"name" : "Youtube Link",
					"command" : "play"
				};
				playVideo(strng);
			});
			$('#findAndDownload').click(function() {
				var strng = {
					"name" : $("#name").val()
				};
				findAndDownload(strng);
			});
		});

		function getName() {
			var startInd = $("#mgn").val().indexOf("dn=") + 3;
			var endInd = $("#mgn").val().indexOf("&tr=");
			var name = jQuery('#mgn').val().substring(startInd, endInd);

			return name.replace(/\+/g, " ");
		}

		function playMagnet(parameter) {
			$.ajax({
				type : "POST",
				url : 'kudufy/magnet/watch',
				data : JSON.stringify(parameter),
				contentType : "application/json; charset=utf-8",
				dataType : "json"
			});
		}
		function downloadMagnet(parameter) {
			$.ajax({
				type : "POST",
				url : 'kudufy/magnet/download',
				data : JSON.stringify(parameter),
				contentType : "application/json; charset=utf-8",
				dataType : "json"
			});
		}
		function playVideo(parameter) {
			$.ajax({
				type : "POST",
				url : 'kudufy/video/watch',
				data : JSON.stringify(parameter),
				contentType : "application/json; charset=utf-8",
				dataType : "json"
			});
		}
		function findAndDownload(parameter) {
			$.ajax({
				type : "POST",
				url : 'kudufy/video/findanddownload',
				data : JSON.stringify(parameter),
				contentType : "application/json; charset=utf-8",
				dataType : "json"
			});
		}