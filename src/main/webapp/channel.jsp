
<div class="page-header">
	<h1>Kudu Tansu Channels</h1>
</div>
<%
    String isConnected = request.getParameter("isConnected");
%>

<h5>
	Kudu Status
	<%
    if (isConnected.equals("true")) {
%>
	<span class="label label-success">Connected</span>
	<%
	    } else {
	%>
	<span class="label label-danger">Disconnected</span>
	<%
	    }
	%>
</h5>


<div class="page-header">
	<h3>Custom Magnet Link Player</h3>
</div>
<div>
	<form name="magnet">
		<div>
			Magnet URI: <input type="text" class="form-control" name="magnetUri"
				id="mgn" /> <br> Subtitle: <input type="text"
				class="form-control" name="subtitleUrl" id="srt" /><br>
		</div>
	</form>
	<input id="watchMagnet" class="btn btn-default" type="button"
		name="Watch" value="Watch" /> <input id="downloadMagnet"
		class="btn btn-default" type="button" name="Download" value="Download" />
</div>

<div class="page-header">
	<h3>Custom Video Player</h3>
</div>
<div>
	<form name="link">
		<div>
			Video: <input type="text" class="form-control" name="videoUrl"
				id="videoLink" /> <br>
		</div>
	</form>
	<input id="playVideo" class="btn btn-default" type="button" name="Play"
		value="Play" />
</div>

<div class="page-header">
	<h3>Find and Download</h3>
</div>
<div>
	<form name="link">
		<div>
			Search Name: <input type="text" class="form-control" name="name"
				id="name" /> <br>
		</div>
	</form>
	<input id="findAndDownload" class="btn btn-default" type="button"
		name="Find And Download" value="Find And Download" />
</div>

<div class="page-header">
	<h3>Torrent Links</h3>
</div>
<div>
	<div>
		<a href="#" class="openpage" data-url="http://startorrents.com/">Star
			Torrent</a><br> <a href="#" class="openpage"
			data-url="http://www.opensubtitles.org/tr">Open Subtitles</a>
	</div>
</div>
